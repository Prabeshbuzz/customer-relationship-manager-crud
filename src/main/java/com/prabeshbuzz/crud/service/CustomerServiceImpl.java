package com.prabeshbuzz.crud.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prabeshbuzz.crud.model.Customer;
import com.prabeshbuzz.crud.repository.CustomerRepository;
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

	@Autowired 
	CustomerRepository customerRepository;
	
	@Override
	public List<Customer> getAllCustomer() {
		return (List<Customer>)customerRepository.findAll();
	}

	@Override
	public Customer getCustomerbyId(long id) {
		return customerRepository.findById(id).get();
		}

	@Override
	public void saveOrUpdate(Customer customer) {
		customerRepository.save(customer);
		
	}

	@Override
	public void deleteCustomer(long id) {
		customerRepository.deleteById(id);
		
	}

}
