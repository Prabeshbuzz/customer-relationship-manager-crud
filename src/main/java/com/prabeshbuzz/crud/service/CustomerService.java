package com.prabeshbuzz.crud.service;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.prabeshbuzz.crud.model.*;

public interface CustomerService {
	
	public List<Customer> getAllCustomer();
	
	public Customer getCustomerbyId(long id);
	
	public void saveOrUpdate(Customer customer);
	
	public void deleteCustomer( long id);
	
	
	
}
