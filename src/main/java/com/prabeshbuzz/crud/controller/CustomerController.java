package com.prabeshbuzz.crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.prabeshbuzz.crud.model.Customer;
import com.prabeshbuzz.crud.service.CustomerService;

@Controller
@RequestMapping("/customer")
public class CustomerController {
	@Autowired
	CustomerService customerService;

	@GetMapping("/list")
	public String listCustomers(Model theModel) {
		List<Customer> theCustomers = customerService.getAllCustomer();
		theModel.addAttribute("customers", theCustomers);
		return "customer-list";
	}

	@GetMapping(value = "/showFormForAdd")
	public String showFormForAdd(Model theModel) {
		// create new model attribute to bind new data
		Customer theCustomer = new Customer();
		theModel.addAttribute("customer", theCustomer);

		return "customer-form";
	}
	
	@GetMapping("/showFormForUpdate") 
	public String showFormForUpdate(@RequestParam("customerId") int id, Model theModel) {
		//get customer from db
		Customer theCustomer = customerService.getCustomerbyId(id);
		
		//set customer as a model attribute
		theModel.addAttribute("customer",theCustomer);
		//send to the form
		
		return "customer-form";
	}
	
	@GetMapping(value="/deleteCustomer")
	public String deleteCustomer(@RequestParam("customerId") Long id) {
		customerService.deleteCustomer(id);
		return "redirect:/customer/list";
	}
	
	@PostMapping("/saveCustomer")
	public String saveCustomer(@ModelAttribute("customer") Customer customer) {
		customerService.saveOrUpdate(customer);
		return "redirect:/customer/list";
	} 
	
}