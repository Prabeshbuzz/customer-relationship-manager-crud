<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<link href="../../webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet" />
<script src="../../webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="../../webjars/jquery/3.0.0/js/jquery.min.js"></script>
<link href="/css/main.css" rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<!--    navigation bar  -->

	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<a class="navbar-brand" href="#">PrabeshBuzz</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
				<li class="nav-item active"><a class="nav-link" href="#">Home
						<span class="sr-only">(current)</span>
				</a></li>
				<li class="nav-item"><a class="nav-link" href="#">About</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Contact
						us</a></li>
			</ul>
		</div>
	</nav>
	<h3>Customer Relationship Manager</h3>
	<div class="container">

		<input type="button" value="Add Customer"
			onClick="window.location.href='showFormForAdd';return false;" />
		<hr>
		<table border= 2px solid blue class="table">
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>SSN</th>
				<th>PhoneNumber</th>
				<th>Update/Delete</th>
			</tr>
			<c:forEach items="${customers}" var="cust" >
			
			<c:url var ="updateLink" value="/customer/showFormForUpdate">
				<c:param name="customerId" value="${cust.id}"/>
			</c:url>
			
			<c:url var ="deleteLink" value="/customer/deleteCustomer">
				<c:param name="customerId" value="${cust.id}"/>
			</c:url>
			
			<tr>
			<td>${cust.id}</td>
			<td>${cust.name}</td>
			<td>${cust.ssn}</td>
			<td>${cust.phoneNumber}</td>
			<td>
			<a href="${updateLink}"> Update</a>
			<a href="${deleteLink}"> Delete</a>
			
			</td>
			</tr>
			</c:forEach>
		</table>

	</div>


</body>
</html>