<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<link href="../../webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet" />
<script src="../../webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="../../webjars/jquery/3.0.0/js/jquery.min.js"></script>
<link href="/css/main.css" rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Save Customer</title>
</head>
<body>
	<!--    navigation bar  -->

	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<a class="navbar-brand" href="#">PrabeshBuzz</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
				<li class="nav-item active"><a class="nav-link" href="#">Home
						<span class="sr-only">(current)</span>
				</a></li>
				<li class="nav-item"><a class="nav-link" href="#">About</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Contact
						us</a></li>
			</ul>
		</div>
	</nav>
	
	<div class="container">
		<h3> Save Customer</h3>
		<form:form action="saveCustomer" modelAttribute="customer" method="POST">
		<table>
			<tbody>
				<tr>
					<td><label>Name:</label></td>
					<td><form:input path="name"/> </td>
				</tr>
				<tr>
					<td><label>SSN:</label></td>
					<td><form:input path="ssn"/> </td>
				</tr>
				<tr>
					<td><label>PhoneNumber:</label></td>
					<td><form:input path="phoneNumber"/> </td>
				</tr>
				<tr>
					<td><label></label></td>
					<td><input type="submit" value="save"  class="btn btn-primary"/> </td>
				</tr>
			</tbody>
		</table>
		
		</form:form>
		<p>
		<a href="/customer/list"> click to go back to list</a>
		</p>
	</div>
	
	

</body>
</html>